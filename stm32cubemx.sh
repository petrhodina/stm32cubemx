#!/bin/sh

STM32CUBEMX_URL="http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/resource/technical/software/sw_development_suite/stm32cubemx.zip"
STM32CUBEMX_ZIP="stm32cubemx.zip"
STM32CUBEMX_ALIAS="stm32cubemx"

echo "Downloading STM32CubeMX ..."

wget -O $STM32CUBEMX_ZIP $STM32CUBEMX_URL

echo "Extracting installer ..."
unzip $STM32CUBEMX_ZIP

echo "Launching installer"

sudo java -jar SetupSTM32CubeMX-?.?.?.exe

echo "Creating alias stm32cubemx ..."
echo -e "alias $STM32CUBEMX_ALIAS=\"java -jar /usr/local/STMicroelectronics/STM32Cube/STM32CubeMX/STM32CubeMX.exe\"" >> ~/.bashrc

echo "Removing temporary files ..."
rm $STM32CUBEMX_ZIP SetupSTM32CubeMX-?.?.?.exe
